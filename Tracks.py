from Gaudi.Configuration import *
from Configurables import Kepler
#Kepler().EvtMax = 2000
eospath = "/eos/lhcb/testbeam/velo/timepix3/June2017/"
run = "21017"
Kepler().InputFiles = [eospath + 'RawData/Run' + run + '/']
Kepler().AlignmentFile = eospath + 'RootFiles/Run' + run + '/Conditions/Alignment' + run + 'mille.dat'

from Configurables import TbEventBuilder
TbEventBuilder().MinPlanesWithHits = 5

from Configurables import TbSimpleTracking
TbSimpleTracking().MinPlanes = 7

Kepler().WriteTuples = True
from Configurables import TbTupleWriter
TbTupleWriter().WriteTriggers = True
TbTupleWriter().WriteHits = False
TbTupleWriter().WriteClusters = False
TbTupleWriter().WriteTracks = True
TbTupleWriter().StrippedNTuple = True

